// Author : Umesh Mohan (moh@nume.sh)
// Works with attiny (<500 bytes hex file)

#define F_CPU 1000000UL
#define spike_width 3 // ms
#define intra_burst_delay 5 // ms
#define spike_bursting_probability 0x9999 // 0.6 * 0xFFFF
#define max_quiescent_period_multiplier 65 // 0xFFFF / 65 ~= 1008 ms

#include <avr/io.h>
#include <util/delay.h>

// Adapted from https://blog.podkalicki.com/attiny13-pseudo-random-numbers/
// because the rand() from stdlib.h takes up too much space on chip!

static uint16_t random_number = 0xabcd;
uint16_t random(void)
{
  random_number = (random_number >> 1) ^ (-(random_number & 1) & 0xB400U);
  return random_number;
}

void delay_ms(long delay) // bug? unable to use _delay_ms with dynamic values
{
  for (volatile long i = 0; i < delay; i++)
  {
    _delay_ms(1);
  }
}

void spike()
{
  PORTB |= (1 << PB0); // set PB0 as high
  _delay_ms(spike_width);
  PORTB &= ~(1 << PB0); // set PB0 as low
  if (random() < spike_bursting_probability)
  {
    _delay_ms(intra_burst_delay);
    spike();
  }
}

int main(void)
{
  DDRB |= (1 << PB0); // set PB0 as output  
  while (1)
  {
    spike();
    delay_ms(random() / max_quiescent_period_multiplier);
  }
}
