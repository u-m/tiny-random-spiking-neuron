EESchema Schematic File Version 4
LIBS:attiny85-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "tiny random spiking neuron (attiny85)"
Date ""
Rev "0.1"
Comp ""
Comment1 "Author: Umesh Mohan (moh@nume.sh)"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_POT_TRIM RV1
U 1 1 5D1E5473
P 7050 4450
F 0 "RV1" H 6980 4496 50  0000 R CNN
F 1 "100k" H 6980 4405 50  0000 R CNN
F 2 "Potentiometer_THT:Potentiometer_ACP_CA9-H5_Horizontal" H 7050 4450 50  0001 C CNN
F 3 "~" H 7050 4450 50  0001 C CNN
	1    7050 4450
	1    0    0    -1  
$EndComp
Text Label 6200 4250 0    50   ~ 0
PB0
Text Label 6200 4350 0    50   ~ 0
PB1
Text Label 6200 4450 0    50   ~ 0
PB2
Text Label 6200 4550 0    50   ~ 0
PB3
$Comp
L power:VCC #PWR0101
U 1 1 5D1E5536
P 5600 3950
F 0 "#PWR0101" H 5600 3800 50  0001 C CNN
F 1 "VCC" H 5617 4123 50  0000 C CNN
F 2 "" H 5600 3950 50  0001 C CNN
F 3 "" H 5600 3950 50  0001 C CNN
	1    5600 3950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5D1E55A6
P 5600 5150
F 0 "#PWR0102" H 5600 4900 50  0001 C CNN
F 1 "GND" H 5605 4977 50  0000 C CNN
F 2 "" H 5600 5150 50  0001 C CNN
F 3 "" H 5600 5150 50  0001 C CNN
	1    5600 5150
	1    0    0    -1  
$EndComp
Text Label 7050 4300 0    50   ~ 0
PB0
$Comp
L power:GND #PWR0105
U 1 1 5D1E56BC
P 7050 4600
F 0 "#PWR0105" H 7050 4350 50  0001 C CNN
F 1 "GND" H 7055 4427 50  0000 C CNN
F 2 "" H 7050 4600 50  0001 C CNN
F 3 "" H 7050 4600 50  0001 C CNN
	1    7050 4600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5D1E5703
P 7400 4450
F 0 "J3" H 7373 4423 50  0000 R CNN
F 1 "Out" H 7373 4332 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7400 4450 50  0001 C CNN
F 3 "~" H 7400 4450 50  0001 C CNN
	1    7400 4450
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5D1E5718
P 7200 4550
F 0 "#PWR0106" H 7200 4300 50  0001 C CNN
F 1 "GND" H 7205 4377 50  0000 C CNN
F 2 "" H 7200 4550 50  0001 C CNN
F 3 "" H 7200 4550 50  0001 C CNN
	1    7200 4550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0107
U 1 1 5D1E57B7
P 6050 2700
F 0 "#PWR0107" H 6050 2550 50  0001 C CNN
F 1 "VCC" H 6067 2873 50  0000 C CNN
F 2 "" H 6050 2700 50  0001 C CNN
F 3 "" H 6050 2700 50  0001 C CNN
	1    6050 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5D1E57BD
P 6050 3000
F 0 "#PWR0108" H 6050 2750 50  0001 C CNN
F 1 "GND" H 6055 2827 50  0000 C CNN
F 2 "" H 6050 3000 50  0001 C CNN
F 3 "" H 6050 3000 50  0001 C CNN
	1    6050 3000
	1    0    0    -1  
$EndComp
$Comp
L MCU_Microchip_ATtiny:ATtiny85-20SU U1
U 1 1 5D202A10
P 5600 4550
F 0 "U1" H 5070 4596 50  0000 R CNN
F 1 "ATtiny85-20SU" H 5070 4505 50  0000 R CNN
F 2 "Package_SO:SOIJ-8_5.3x5.3mm_P1.27mm" H 5600 4550 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 5600 4550 50  0001 C CNN
	1    5600 4550
	1    0    0    -1  
$EndComp
Text Label 6200 4650 0    50   ~ 0
PB4
Text Label 6200 4750 0    50   ~ 0
PB5
$Comp
L Connector:Conn_01x08_Male J2
U 1 1 5D202DC4
P 5550 2900
F 0 "J2" H 5656 3378 50  0000 C CNN
F 1 "attiny85" H 5656 3287 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x08_P1.27mm_Vertical" H 5550 2900 50  0001 C CNN
F 3 "~" H 5550 2900 50  0001 C CNN
	1    5550 2900
	1    0    0    -1  
$EndComp
Text Label 5750 2900 0    50   ~ 0
PB0
Text Label 5750 2800 0    50   ~ 0
PB1
Text Label 5750 2600 0    50   ~ 0
PB2
Text Label 5750 3300 0    50   ~ 0
PB3
Text Label 5750 3100 0    50   ~ 0
PB4
Text Label 5750 3200 0    50   ~ 0
PB5
Wire Wire Line
	5000 3000 5100 3000
Wire Wire Line
	5000 2900 5100 2900
$Comp
L power:GND #PWR0104
U 1 1 5D1E55D2
P 5100 3000
F 0 "#PWR0104" H 5100 2750 50  0001 C CNN
F 1 "GND" H 5105 2827 50  0000 C CNN
F 2 "" H 5100 3000 50  0001 C CNN
F 3 "" H 5100 3000 50  0001 C CNN
	1    5100 3000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0103
U 1 1 5D1E55C2
P 5100 2900
F 0 "#PWR0103" H 5100 2750 50  0001 C CNN
F 1 "VCC" H 5117 3073 50  0000 C CNN
F 2 "" H 5100 2900 50  0001 C CNN
F 3 "" H 5100 2900 50  0001 C CNN
	1    5100 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5D1E51E1
P 4800 2900
F 0 "J1" H 4906 3078 50  0000 C CNN
F 1 "Battery" H 4906 2987 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4800 2900 50  0001 C CNN
F 3 "~" H 4800 2900 50  0001 C CNN
	1    4800 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 2700 6050 2700
Wire Wire Line
	5750 3000 6050 3000
$EndSCHEMATC
